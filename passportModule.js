
const userJs = require('./models/user.js');

const configJs = require('./config.js');

const crypto = require('crypto');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const BasicStrategy = require('passport-http').BasicStrategy;
const serverSalt = configJs.hashingSalt;










 // local strategy
passport.serializeUser(onSerialize);
passport.deserializeUser(onDeserialize);
function onSerialize(user, doneCB) {
doneCB(null, user.id);
}
function onDeserialize(id, doneCB) {
 
  userJs.User.getById(id).then(user => {
    if (!user) {
      doneCB("No user");
    }
    else {
   doneCB(null, user);
  }

  })
    .catch(err => res.status(500).send("We have some problem! oops..."))

}
passport.use(new LocalStrategy(onLogin));
function onLogin(username, password, doneCB) {
  const hashedPass = sha512(password, serverSalt).passwordHash;
  userJs.User.getByLoginAndHashedPass(username, hashedPass).then(result => {
    if (result) {
     
      doneCB(null, result)
    }
    else {
      doneCB(null, false);
    }
  }).catch(err => console.error(err));
}
////local strategy//////



////basic strategy


passport.use(new BasicStrategy(
    function (login, password, done) {
        const hashedPass = sha512(password, serverSalt).passwordHash;

        userJs.User.getByLoginAndHashedPass(login, hashedPass)
            .then(user => {
                if (user) {
                    return done(null, user);
                }
                else {
                    return done(null, false);
                }
            })
            .catch(error => { return done(err); })
    }
));


//function for coding in local strategy and basic strategy
function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
      salt: salt,
      passwordHash: value
    };
  }

module.exports = {sha512}