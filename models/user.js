let fs = require('fs').promises
let mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    role: { type: String, default: "user" },
    login: { type: String, required: true },
    hashedPassword: { type: String, required: false },
    
    name: { type: String, default: " " },
    surname: { type: String, default: " " },

    registeredAt: { type: Date, default: Date.now },
    avaUrl: { type: String },
    isDisabled: { type: Boolean },
    public_id: { type: String },
    email: {type: String}
    


    
});
const UserModel = mongoose.model('User', UserSchema);
class User {

    static insert(x) {
        return new UserModel(x).save();
    }
    static getAll() {
        return UserModel.find();
    }
    static getByLoginAndHashedPass(username, hashedPassword) {
        return UserModel.findOne({ login: username, hashedPassword: hashedPassword });
    }
    static getById(id) {
        if (mongoose.Types.ObjectId.isValid(id)) {
            return UserModel.findById(id);
        }
        else {
            return Promise.reject("(Id is not valid)IT`s good trying, friend). But not today!");
        }
    }
    static getByLogin(username) {
        return UserModel.findOne({ login: username });
    }
    static delete(id) {
        return UserModel.findByIdAndDelete(id)
    }
    static update(x) {
        console.log(x);

       

            if (x.avaUrl == undefined) {
               
                return UserModel.findByIdAndUpdate(x.id, {name: x.name, surname: x.surname , role: x.role}, { new: true })
            }
            else {
                console.log("hello kuki ");
                console.log(x + "our undated post!!!!!!!!");
                return UserModel.findByIdAndUpdate(x.id, {  name: x.name, surname: x.surname , avaUrl: x.avaUrl, public_id: x.public_id }, { new: true })
            }
        }
    static updateRole(x) {
        console.log(x);
        return UserModel.updateOne({ _id: x.id }, { role: x.role });
    }
}
module.exports = { User };



