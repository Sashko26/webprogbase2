

let fs = require('fs').promises;
let mongoose = require('mongoose');


const CommentSchema = new mongoose.Schema({
   
    content: { type: String },
    idOfOwner: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    idOfPost: { type: mongoose.Schema.Types.ObjectId, ref: 'Post' },
    dateOfpublication: { type: Date, default: Date.now },
    avaUrl: { type: String },
    public_id: {type: String},
    userLogin: {type: String},
    deleteIcon: { type: Boolean}
});
const CommentModel = mongoose.model("Comment", CommentSchema);


class Comment {

    constructor(idOfOwner = null, idOfPost = null, content = null,dateOfpublication = null) {
        this.content = content;
        this.dateOfpublication = dateOfpublication;
        this.idOfOwner = idOfOwner;
        this.idOfPost = idOfPost;
       
        this.dateOfpublication = new Date();
        this.deleteIcon = false;
    }


      static  getAllWithPagination(limit,idOfPost)
      {
          return CommentModel.find({idOfPost : idOfPost}).limit(limit);
      }

    static deleteAllCommentsThatBelongUser(idOfOwner)
    {
        return CommentModel.remove({idOfOwner:idOfOwner})
    }
    static deleteAllCommentsThatBelongPost(idOfPost)
    {
        return CommentModel.remove({idOfPost:idOfPost})
    }

    static insert(x) {
        return new CommentModel(x).save();
    }
    static getAllThatBelongToUser(idOfOwner)
    {
        return CommentModel.find({ idOfOwner: idOfOwner});
    }
    static getAll(idOfPost = null) {
        if (idOfPost === null) {
            return CommentModel.find();
        }
        else {
            return CommentModel.find({ idOfPost: idOfPost });
        }

    }
    static getById(id) {
        if(mongoose.Types.ObjectId.isValid(id))
        {
            return CommentModel.findById(id);
        }
        else{
            return Promise.reject("IT`s good trying, friend). But not today!");
        }
        
    }
    static update(x) {


        if (x.pictureUrl == undefined) {
            return CommentModel.findByIdAndUpdate(x.id, { name: x.name, content: x.content });
        }
        else {
            return CommentModel.findByIdAndUpdate(x.id, { name: x.name, content: x.content, pictureUrl: x.pictureUrl , public_id : x.public_id},{new: true});
        }




    }
    static deleteById(id) {
        return CommentModel.findByIdAndDelete(id);
    }
}
module.exports = { Comment };