
let fs = require('fs').promises;
let mongoose = require('mongoose');

const PhotoSchema = new mongoose.Schema({

    idOfOwner: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    dateOfpublication: { type: Date, default: Date.now },
    pictureUrl: { type: String },
    public_id: { type: String }


});
const PhotoModel = mongoose.model("Photo", PhotoSchema);
class Photo {

  




    static  getAllWithPagination(limit,idOfUser)
    {
        return PhotoModel.find({idOfOwner : idOfUser}).limit(limit);
    }

   
    static insert(x) {

        return new PhotoModel(x).save();
    }

    static getAllThatBelongToUser(idOfOwner) {
        return PhotoModel.find({ idOfOwner: idOfOwner });
    }
    static getById(id) {
        if (mongoose.Types.ObjectId.isValid(id)) {
            return PhotoModel.findById(id);
        }
        else {
            return Promise.reject("IT`s good trying, friend). But not today!");
        }

    }
    static update(x) {

        if (x.pictureUrl == undefined) {
            
            return PostModel.findByIdAndUpdate(x.id, { name: x.name, content: x.content });
        }
        else {
          
            return PostModel.findByIdAndUpdate(x.id, { name: x.name, content: x.content, pictureUrl: x.pictureUrl, public_id: x.public_id }, { new: true });
        }
    }
    static deleteById(id) {
        return PhotoModel.findByIdAndDelete(id);
    }
}
module.exports = { Photo, PhotoModel, PhotoSchema };