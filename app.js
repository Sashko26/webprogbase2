

const busboyBodyParser = require('busboy-body-parser');
const bodyParser = require("body-parser");
const mustache = require('mustache-express');
const userJs = require('./models/user.js');
const photoJs = require('./models/photo.js');
const commentJs = require('./models/comment.js');

const postJs = require('./models/post.js');
const additionalJs = require('./models/additional.js');
const express = require("express");
const path = require('path');

let mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const crypto = require('crypto');
const passport = require('passport');



const configJs = require('./config.js');
const serverSalt = configJs.hashingSalt;

const app = express();
require('./passportModule');



const connectOptions = { useNewUrlParser: true };
mongoose.connect(configJs.DataBaseUrl, connectOptions)
  .then(() => console.log('Mongo database connected'))
  .catch(() => console.log('ERROR: Mongo database not connected'));




///застарілий mongoDB
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

/* const configJs = require('./config'); */

const cloudinary = require('cloudinary').v2;
cloudinary.config({
  cloud_name: configJs.cloud_name,
  api_key: configJs.api_key,
  api_secret: configJs.api_secret
});


app.use(busboyBodyParser());

app.use(busboyBodyParser(
  {
    limit: "5mb",
    multi: true,
  }
));
app.use(express.static('public'));
const viewsDir = path.join(__dirname, 'views');

app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');


app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({
  secret: configJs.sessionSecret, // 'SEGReT$25_'
  resave: false,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.set('view engine', 'mst');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(busboyBodyParser({ limit: '5mb' }));
// new middleware
app.use(bodyParser.urlencoded({ extended: true }));
const auth = require('./auth');
const developer = require('./routes/developer');
const api = require('./routes/api');

app.use('/auth', auth);
app.use('/developer/v1', developer);
app.use('/api/v1', api);

app.post('/deleteComment', function (req, res) {
  commentJs.Comment.deleteById(req.body.id)
    .then(comment => {
      res.redirect('posts/' + req.body.idOfPost);
    })
    .catch(err => {
      res.send(err);
    });
});

app.get("/data/fs/:name", (req, res) => {
  let name = "/data/fs/" + req.params.name;
  res.sendFile(__dirname + name);
});
app.post('/confirmUpdateComment', function (req, res) {

  let id = req.body.id;
  let name = req.body.name;
  let content = req.body.content;
  if (req.files.photoFile == undefined || ((req.files.photoFile.name.indexOf('.jpeg') + 1) == 0 && (req.files.photoFile.name.indexOf('.png') + 1) == 0 && (req.files.photoFile.name.indexOf('.jpg') + 1) == 0)) {
    let x = { id: id, name: name, content: content }
    commentJs.Comment.update(x)
      .then(comment => { res.redirect(`comments/${comment.id}`) })
      .catch(err => { console.log(err) });
  }
  else {
    let promise1 = new Promise((resolve, reject) => {
      cloudinary.uploader.upload_stream({ resource_type: 'raw' }, function (error, result) {
        if (error)
          reject(error)
        else
          resolve(result)
      }).end(req.files.photoFile.data);
    })
    promise1.then(result => {

      let pictureUrl = result.secure_url;
      let x = { id: id, name: name, content: content, pictureUrl: pictureUrl };
      commentJs.Comment.update(x)
        .then(comment => { res.redirect(`comments/${comment.id}`) })
        .catch(err => { console.log(err) });
    })
  }
})


app.get('/posts', function (req, res) {

  if (req.user) {
    if (req.user.login == 'admin') {
      res.status(200).render('posts', { admin: req.user.login });
    }
    else {
      res.status(200).render('posts', { user: req.user.login });
    }

  }
  else {

  }


});

app.post('/addPhoto', function (req, res) {

  if (req.files.photoFile) {
    let promise1 = new Promise((resolve, reject) => {
      cloudinary.uploader.upload_stream({ resource_type: 'raw' }, function (error, result) {
        if (error)
          reject(error)
        else
          resolve(result)
      }).end(req.files.photoFile.data);
    })
    promise1.then(result => {
      console.log("come on every body");

      let x = { idOfOwner: req.body.user_id, public_id: result.public_id, pictureUrl: result.secure_url }
      photoJs.Photo.insert(x)
        .then(post => { res.redirect(`profile`) })
        .catch(err => { console.log(err) });
    })
  }
  else {
    res.redirect(`profile`);
  }





})
app.post('/confirmUpdateUser', function (req, res) {
  if (req.user) {
    let name = req.body.newName;
    let surname = req.body.newSurname;




    if (name.trim().length == 0) {
      name = req.user.name;
    }
   





    if (surname.trim().length == 0) {
      surname = req.user.surname;
    }
  


    if (req.files.newAvatar == undefined || ((req.files.newAvatar.name.indexOf('.jpeg') + 1) == 0 && (req.files.newAvatar.name.indexOf('.png') + 1) == 0 && (req.files.newAvatar.name.indexOf('.jpg') + 1) == 0)) {
      let x = { id: req.user._id, name: name, surname: surname, role: req.user.role, public_id: req.user.public_id, avaUrl: req.user.avaUrl };
      userJs.User.update(x)
        .then(user => {
          res.redirect('/profile')
        })
        .catch(err => { console.log(err) });
    }
    else {


      let promise1 = new Promise((resolve, reject) => {
        cloudinary.uploader.upload_stream({ resource_type: 'raw' }, function (error, result) {
          if (error)
            reject(error)
          else
            resolve(result)
        }).end(req.files.newAvatar.data);
      })
      promise1.then(result => {
        console.log("come on every body");
        let pictureUrl = result.secure_url;
        let x = { id: req.user.id, name: name, surname: surname, role: req.user.role, public_id: result.public_id, avaUrl: result.secure_url }
        userJs.User.update(x)
          .then(post => { res.redirect(`profile`) })
          .catch(err => { console.log(err) });
      })
    }
  }
  else {

  }
})
app.post('/confirmUpdatePost', function (req, res) {
  let id = req.body.id;
  let name = req.body.name;
  let content = req.body.content;
  if (req.files.photoFile == undefined || ((req.files.photoFile.name.indexOf('.jpeg') + 1) == 0 && (req.files.photoFile.name.indexOf('.png') + 1) == 0 && (req.files.photoFile.name.indexOf('.jpg') + 1) == 0)) {
    let x = { id: id, name: name, content: content }
    postJs.Post.update(x)
      .then(post => {
        res.redirect(`posts/${post.id}`)
      })
      .catch(err => { console.log(err) });
  }
  else {
    console.log(req.files.photoFile.name + "huyaka show!!!!!!!!!");

    let promise1 = new Promise((resolve, reject) => {
      cloudinary.uploader.upload_stream({ resource_type: 'raw' }, function (error, result) {
        if (error)
          reject(error)
        else
          resolve(result)
      }).end(req.files.photoFile.data);
    })
    promise1.then(result => {
      console.log("come on every body");
      let pictureUrl = result.secure_url;
      let x = { id: id, name: name, content: content, pictureUrl: pictureUrl };
      postJs.Post.update(x)
        .then(post => { res.redirect(`posts/${post.id}`) })
        .catch(err => { console.log(err) });
    })
  }
})



app.post('/updatePost', function (req, res) {
  if (req.user) {
    if (req.user.role == "admin") {
      postJs.Post.getById(req.body.id)
        .then(post => {
          res.render('updatePost', { post: post, admin: req.user.login })
        })
        .catch(err => res.status(404).render('notFound'))
    }
    else {
      postJs.Post.getById(req.body.id)
        .then(post => {
          if (post.idOfOwner != req.user.id) {
            res.render('index', { user: req.user.login });
          }
          else {
            res.render('updatePost', { post: post, user: req.user.login })
          }
        })

    }


  }
  else {

  }

});

app.post('/updateComment', function (req, res) {
  commentJs.Comment.getById(req.body.id).
    then(comment => {
      res.render('updateComment', { comment: comment })
    })
});


function sha512(password, salt) {
  const hash = crypto.createHmac('sha512', salt);
  hash.update(password);
  const value = hash.digest('hex');
  return {
    salt: salt,
    passwordHash: value
  };
}
app.post('/delete', (req, res) => {
  postJs.Post.deleteById(req.body.id)
    .then(data => {

      new Promise((resolve, reject) => {

        if (data.public_id) {
          cloudinary.uploader.destroy(data.public_id, { invalidate: true, resource_type: "raw" }, function (error, result) {
            if (error)
              reject(error)
            else
              resolve(result)
          })
        }
        else {
          resolve(null);
        }
      })
        .then(result => {
          console.log(result)
          res.redirect('posts')
        })
        .catch(error => {
          console.log(error);
          res.send("fignya");
        })
        .catch(err => res.send(err));
    })
});







app.post('/addComment', (req, res) => {
  postJs.Post.deleteById(req.body.id)
    .then(data => {
      res.redirect('posts')
    })
    .catch(err => res.send("We have some problem! oops..."));
});



app.post('/uploadNewComment', function (req, res) {

  ////хардкод
  let idOfOwner = req.user.id;
  ////хардкод
  let postId = req.body.id;
  let content = req.body.contentComment;

  if (content.length > 200) {
    content = content.substring(0, 200);
  }




  let object = new commentJs.Comment(idOfOwner, postId, content);

  return commentJs.Comment.insert(object)
    .then(result => {

      res.redirect(`posts/${postId}`);
    }).catch(err => {
      res.status(500).send("We have some problem! oops...");
      return;
    });
})
app.post('/upload', function (req, res) {
  if (req.body.content.length > 200) {
    req.body.content = req.body.content.substring(0, 200);
  }





  if (req.files.photoFile && ((req.files.photoFile.name.indexOf('.jpeg') + 1) != 0 || (req.files.photoFile.name.indexOf('.png') + 1) != 0 || (req.files.photoFile.name.indexOf('.jpg') + 1) != 0)) {
    new Promise((resolve, reject) => {
      cloudinary.uploader.upload_stream({ resource_type: 'raw' }, function (error, result) {
        if (error)
          reject(error)
        else
          resolve(result)
      }).end(req.files.photoFile.data)
    }).then(result => {
      return additionalJs.getPostFromRequestFromCreatingNewPost(req.user.id, req.body.name, result.secure_url, req.body.content, result.public_id);
    }).then(result => {
      console.log(result);
      res.redirect(`/posts/${result._id}`);
      console.log(result);
    }).catch(err => {
      console.log(err);
      res.status(500).send("We have some problem! oops...");
      return;
    });
  }
  else {
    let post = { idOfOwner: req.user.id, name: req.body.name, content: req.body.content };
    postJs.Post.insert(post)
      .then(result => {
        res.redirect(`/posts/${result._id}`);
      }).catch(err => {
        console.log(err);
        res.status(500).send("We have some problem! oops...");
        return;
      });

  }

});


function makeGetter(name) {
  app.get('/' + name, function (req, res) {
    if (req.user == undefined) {
      res.render(name, { guest: 'guest' });
    }
    else if (req.user.role == 'admin') {
      res.render(name, { admin: req.user.login });
    }
    else {
      res.render(name, { user: req.user.login });
    }


  });
}
function forEach__(list, ln) {
  for (let el of list) {
    ln(el);
  }
}
forEach__(["about"], makeGetter);


app.get('/photos/:id', function (req, res) {
  if (req.user) {
    if (req.user.role == "admin") {
      console.log(req.params.id);
      photoJs.getById(req.params.id)
        .then(photo => { res.render('photo', { admin: req.user.login, photo: photo }); })


    }
    else {
      console.log(req.params.id);
      photoJs.Photo.getById(req.params.id)
        .then(photo => { res.render('photo', { user: req.user.login, photo: photo }); })
    }
  }
  else {
    res.redirect('/auth/register');
  }

})
app.post('/deletePhoto', function (req, res) {
  if (req.user) {
    photoJs.Photo.deleteById(req.body.id)
      .then(result => {
        console.log(result);
        res.render('profile', { admin: req.user.login, body: req.user });
      })
  }
  else {
    res.redirect('/auth/register');
  }
})

app.get('/profile', function (req, res) {
  if (req.user == undefined) {
    res.redirect('/auth/login');
  }
  else {
    if (req.user.role == 'admin') {
      photoJs.Photo.getAllThatBelongToUser(req.user.id)
        .then(photos => {
          res.render('profile', { admin: req.user.login, body: req.user });
        })
    }
    else {
      photoJs.Photo.getAllThatBelongToUser(req.user.id)
        .then(photos => {
          if (photos.length == 0) {
          }
          res.render('profile', { user: req.user.login, body: req.user, photos: photos });
        })


    }
  }

})

//promise
app.get('/users', function (req, res) {
  if (req.user) {
    if (req.user.role == "admin") {
      userJs.User.getAll()
        .then(
          data => {
            let dataObj = {};
            dataObj.items = data;
            dataObj = additionalJs.PaginationAndSearch(req, dataObj, 'user');
            dataObj.admin = req.user.login;
            res.status(200).render('users', dataObj);
          })
        .catch(
          (err) => {
            res.status(500).send("We have some problem! oops...");
          })
    }
    else {
      res.redirect('/');
    }
  }

  else {
    res.redirect('/');
  }

})
//promise
app.get('/posts', function (req, res) {
  if (req.user) {
    let user = req.user;
    console.log(req.user);
    postJs.Post.getAll()
      .then(data => {
        let dataObj = {};
        dataObj['items'] = data;
        dataObj = additionalJs.PaginationAndSearch(req, dataObj, 'post');
        if (user.role == 'user') {
          dataObj.user = user.login;
          res.status(200).render('posts', dataObj)
        }
        else {
          dataObj.admin = user.login;
          res.status(200).render('posts', dataObj)
        }

      })
      .catch(err => res.status(500).send(err.toString()))
  }
  else {
    res.redirect('/auth/register');
  }

})
app.post('/changeRole', function (req, res) {
  console.log("hello");
  let role;
  if (req.body.role == 'user') {
    role = 'admin';
  }
  if (req.body.role == 'admin') {
    role = 'user';
  }
  let x = { role: role, id: req.body.id }
  console.log(x.id)
  console.log(x);
  userJs.User.updateRole(x)
    .then(user => res.redirect(`users/${x.id}`))
    .catch(error => {
      console.log(error);
      res.send(error);
    })



})

app.get('/comments', function (req, res) {

  if (req.user) {
    if (req.user.role == 'admin') {
      ///хочу вертати логіни юхерів.L0





      console.log(req.user);
      commentJs.Comment.getAll()
        .then(comments => {
          let commentsObj = {};
          commentsObj['items'] = comments;
          commentsObj = additionalJs.PaginationAndSearch(req, commentsObj, 'comment');
          commentsObj.admin = req.user.login;


          let arrayOfPromises = [];
          for (let i = 0; i < commentsObj.items.length; i++) {
            arrayOfPromises[i] = userJs.User.getById(commentsObj.items[i].idOfOwner);
          }


          return Promise.all([commentsObj, arrayOfPromises]);
        })
        .then(([commentsObj, arrayOfPromises]) => {
          return Promise.all(arrayOfPromises)
            .then(arrayOfPromises => {
              for (let i = 0; i < commentsObj.items.length; i++) {
                /*  commentsObj.items[i].avaUrl = arrayOfPromises[i].avaUrl; */
                commentsObj.items[i].login = arrayOfPromises[i].login;
                console.log(commentsObj.items[i].login + "$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                console.log(commentsObj.items[i].name);
              }
              res.status(200).render('comments', commentsObj);
            })
        })







        .catch(err => {
          console.log(err);
          res.status(404).render('notFound', err);
        })

    }
    else {
      console.log(req.user);
      commentJs.Comment.getAllThatBelongToUser(req.user.id)
        .then(comments => {
          console.log(comments.length + " : amount of comments ")
          if (comments.length == 0) {
            res.render("comments", commentsObj);
          }
          console.log(comments + " HU222222222222222222222222222222222222222222222  ");

          let commentsObj = {};
          commentsObj['items'] = comments;
          commentsObj = additionalJs.PaginationAndSearch(req, commentsObj);
          commentsObj.user = req.user.login;
          res.render("comments", commentsObj);
        })
        .catch(err => {
          console.log(err);
          res.status(404).render('notFound', err);
        })

    }

  }
  else {
    res.redirect('/auth/register');
  }

});


app.get('/profile', function (req, res) {
  if (req.user == undefined) {
    res.redirect('/auth/login');
  }
  else {
    if (req.user.role == 'admin') {
      photoJs.Photo.getAllThatBelongToUser(req.user.id)
        .then(photos => {
          res.render('profile', { admin: req.user.login, body: req.user });
        })
    }
    else {
      photoJs.Photo.getAllThatBelongToUser(req.user.id)
        .then(photos => {
          if (photos.length == 0) {
          }
          res.render('profile', { user: req.user.login, body: req.user, photos: photos });
        })


    }
  }

})









app.get('/users/:id', function (req, res) {
  console.log(req.params.id);
  userJs.User.getById(req.params.id)
    .then(user => res.status(200).render('user',{admin:req.user.login, body:user} ))
    .catch(err => res.status(404).render('notFound', err));

});

app.get('/comments/:id', function (req, res) {
  if (req.user) {
    commentJs.Comment.getById(req.params.id)

      .then(comment => {
        let commentObj = {};
        commentObj.comment = comment;
        if (req.user.role = 'admin') {
          commentObj.admin = req.user.login;
        }
        else {
          commentObj.user = req.user.login;
        }
        res.render('comment', commentObj)
      })
      .catch(err => res.send(err));
  }
  else {
    res.redirect('/auth/register');
  }
});

app.get('/index', function (req, res) {
  if (req.user) {
    if (req.user.role == 'admin') {
      res.render('index', { admin: req.user.login });
    }
    else {
      res.render('index', { user: req.user.login });
    }
  }
  else
    res.render('index', { guest: 'guest' });
});

app.get('/posts/new', function (req, res) {
  if (req.user) {
    let objUser = {};
    if (req.user.role == "admin") {
      objUser.admin = req.user.login;
    }
    if (req.user.role == "user") {
      objUser.user = req.user.login;
    }
    res.status(200).render('new', objUser);
  }
  else {
    res.redirect('/auth/register');
  }
}


);
//promise
app.get('/users/:id', function (req, res) {
  if (req.user.role == 'admin') {
    userJs.User.getById(req.params.id)
      .then(user => res.status(200).render('user', user))
      .catch(err => res.status(404).render('notFound', err));
  }




});
app.post('/delete', (req, res) => {
  if (req.user) {
    console.log(req.body.id)
    postJs.Post.deleteById(req.body.id)
      .then(data => {
        res.redirect('posts')
      })
      .catch(err => res.send("We have some problem! oops..."));
  }
  else {
    res.redirect('auth/register');
  }


});
app.get('/posts/:id', function (req, res) {
  let belongPostToUser = false;
  if (req.user) {
    postJs.Post.getById(req.params.id)
      .then(data => {
        console.log();
        console.log("assssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
        console.log(data);
        console.log(data.idOfOwner);
        return Promise.all([data, userJs.User.getById(data.idOfOwner)])
      })
      .then(([data, user]) => {
        console.log(user);
        if (user != null) {
          if (user.avaUrl) {
            data['avaUrl'] = user.avaUrl;
          }

          if (req.user.role == 'admin') {
            belongPostToUser = true;
          }

          console.log(req.user.id);
          console.log(data.idOfOwner);


          if (data.idOfOwner == req.user.id) {
            belongPostToUser = true;
          }
        }
        return Promise.all([data, commentJs.Comment.getAll(data.id)])
      })
      .then(([data, arrayofComments]) => {
        data.nameOfmyPenis = arrayofComments;
        let dataObj = {};
        dataObj['belongPostToUser'] = belongPostToUser;
        dataObj['data'] = data;
        if (req.user.role == 'admin') {
          dataObj['admin'] = req.user.login;
        }
        else if (req.user.role == 'user') {
          dataObj['user'] = req.user.login;
        }

        return dataObj;
      })
      .then(dataObj => {

        let arrayOfPromises = [];

        for (let i = 0; i < dataObj.data.nameOfmyPenis.length; i++) {
          arrayOfPromises[i] = userJs.User.getById(dataObj.data.nameOfmyPenis[i].idOfOwner);
        }
        return Promise.all([dataObj, arrayOfPromises]);
      })
      .then(([dataObj, arrayOfPromises]) => {
        return Promise.all(arrayOfPromises)
          .then(arrayOfPromises => {
            for (let i = 0; i < dataObj.data.nameOfmyPenis.length; i++) {
              dataObj.data.nameOfmyPenis[i].avaUrl = arrayOfPromises[i].avaUrl;
              dataObj.data.nameOfmyPenis[i].login = arrayOfPromises[i].login;

            }


            res.status(200).render('post', dataObj);
          })
      })
      .catch(err => {

        res.status(404).render('notFound', err);
      });
  }
  else {
    res.redirect('auth/register');
  }
});

app.get('/api/users/:id', function (req, res) {
  userJs.User.getById(req.params.id)
    .then(user => {
      user = JSON.stringify(user);
      res.set({ "Content-type": "application/json" }).status(200).send(user)
    })
    .catch(err => res.status(404).render('notFound', err));
});



//lab7
app.get('/',
  (req, res) => {

    let user = req.user;
    if (user === undefined) {
      res.render('index', { guest: 'guest' })
    }
    else if (user.role == 'admin') {
      res.render('index', { admin: user.login });
    }
    else {

      res.render('index', { user: user.login });

    }



  }
);





///ти так експортиш всю парашу, яку ти написав в app.js до www
module.exports = app;

