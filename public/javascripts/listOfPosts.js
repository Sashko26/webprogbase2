window.onload = function () {
    let elementButtonNextPage = document.getElementById("buttonNextPageOfPosts");
    let elementButtonPrevPage = document.getElementById("buttonPrevPageOfPosts");
    let currentPage = 1;
    let amountOfPages;
    console.log("Loading Browser JS app.js");
    Promise.all([
        fetch("/templetes/listOfPosts.mst").then(x => x.text()),
        fetch("/api/v1/postsLocalStrategy").then(x => {
            console.log(x);
            return x.json()
        }),
    ])
        .then(([templateStr, itemsData]) => {

          
         
            console.log('templateStr', templateStr);
            console.log('itemsData', itemsData);
            currentPage=itemsData.page;
            amountOfPages=itemsData.amountOfPages;
            if(currentPage==1)
            {
               elementButtonPrevPage.disabled=true;
            }//
            if(currentPage!=1)
            {
               elementButtonPrevPage.disabled=false;
            }
            if(currentPage==amountOfPages)
            {
                elementButtonNextPage.disabled=true;
            }

            let elemtentAmmontOfPages = document.getElementById('amountOfPage');
            elemtentAmmontOfPages.innerHTML = amountOfPages;
            let elemtentCurrentPage = document.getElementById('currentPage');
            elemtentCurrentPage.innerHTML = currentPage;
            
            amountOfPages = itemsData.amountOfPages;
            console.log(currentPage);
            console.log(amountOfPages);
          /*   const dataObject = { items: itemsData.items }; */


            const dataObject = { items: itemsData.items, isEmpty:itemsData.isEmpty};
            const renderedHtmlStr = Mustache.render(templateStr, dataObject);
            return renderedHtmlStr;
        })
        .then(htmlStr => {
            console.log('htmlStr', htmlStr);
            const appEl = document.getElementById('listOfPosts');
            appEl.innerHTML = htmlStr;
        })
        .catch(err => console.error(err));

    elementButtonNextPage.addEventListener('click', function (event) {
        event.preventDefault();
        currentPage = currentPage + 1;
        Promise.all([
            fetch("/templetes/listOfPosts.mst").then(x => x.text()),
            fetch("/api/v1/postsLocalStrategy?page=" + currentPage).then(x => x.json()),
        ])
            .then(([templateStr,itemsData]) => {
                console.log('templateStr', templateStr);
                console.log('itemsData', itemsData);               
                 amountOfPages = itemsData.amountOfPages;
                const items = { items: itemsData.items, isEmpty:itemsData.isEmpty};
              
              
                const renderedHtmlStr = Mustache.render(templateStr, items);
                let elemtentCurrentPage = document.getElementById('currentPage');
                elemtentCurrentPage.innerHTML = currentPage;
                if(currentPage!=1)
                {
                   elementButtonPrevPage.disabled=false;
                }
                if(currentPage==amountOfPages)
                {
                    elementButtonNextPage.disabled=true;
                }
                return renderedHtmlStr;
            })
            .then(htmlStr => {
                console.log('htmlStr', htmlStr);
                const appEl = document.getElementById('listOfPosts');
                appEl.innerHTML = htmlStr;

            })
            .catch(err => console.error(err));

    }, false);





    let elementButtonEnterSearch = document.getElementById("elementButtonEnterSearch");
    elementButtonEnterSearch.addEventListener('click', function (event) {
        event.preventDefault();
        let searchStr=document.getElementById('searchStrPosts').value;
       
        Promise.all([
            fetch("/templetes/listOfPosts.mst").then(x => x.text()),
            fetch("/api/v1/postsLocalStrategy?searchStr=" + searchStr).then(x => x.json()),
        ])
            .then(([templateStr,itemsData]) => {
              
                console.log('templateStr', templateStr);
                console.log('itemsData', itemsData);               
                 amountOfPages = itemsData.amountOfPages;
                const items = { items: itemsData.items };
                const renderedHtmlStr = Mustache.render(templateStr, items);
                
                let elemtentCurrentPage = document.getElementById('currentPage');
                let elemtentAmmontOfPages = document.getElementById('amountOfPage');
                elemtentAmmontOfPages.innerHTML = amountOfPages;
                elemtentCurrentPage.innerHTML = currentPage;
                if(currentPage!=1)
                {
                   elementButtonPrevPage.disabled=false;
                }
                if(currentPage==amountOfPages)
                {
                    elementButtonNextPage.disabled=true;
                }
                if(currentPage==1)
                {
                   elementButtonPrevPage.disabled=true;
                }
                /* let elemtentAmountPage = document.getElementById('amountOfPage'); */
                return renderedHtmlStr;
            })
            .then(htmlStr => {
                console.log('htmlStr', htmlStr);
                const appEl = document.getElementById('listOfPosts');
                appEl.innerHTML = htmlStr;

            })
            .catch(err => console.error(err));

    }, false);



 elementButtonPrevPage.addEventListener('click', function (event) {
        event.preventDefault();
        currentPage = currentPage - 1;
        Promise.all([
            fetch("/templetes/listOfPosts.mst").then(x => x.text()),
            fetch(`/api/v1/postsLocalStrategy?page=` + currentPage).then(x => x.json()),
        ])
            .then(([templateStr, itemsData]) => {
                console.log('templateStr', templateStr);
                console.log('itemsData', itemsData);
                currentPage = itemsData.page;
                amountOfPages = itemsData.amountOfPages;
                const dataObject = { items: itemsData.items };
                const renderedHtmlStr = Mustache.render(templateStr, dataObject);
                let elemtentCurrentPage = document.getElementById('currentPage');
                elemtentCurrentPage.innerHTML = currentPage;
                if(currentPage == 1)
                {
                    elementButtonPrevPage.disabled = true;
                }
                if(currentPage==amountOfPages)
                {
                    elementButtonNextPage.disabled=true;
                }
                if(currentPage!=amountOfPages)
                {
                    elementButtonNextPage.disabled=false;
                }
                return renderedHtmlStr;
            })
            .then(htmlStr => {
                console.log('htmlStr', htmlStr);
                const appEl = document.getElementById('listOfPosts');
                appEl.innerHTML = htmlStr;
            })
            .catch(err => console.error(err));
    }, false);

}