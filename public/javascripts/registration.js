window.onload = function () {
    //username




    let elementButtonConfirm = document.getElementById("registerButton");

    let elementUserName = document.getElementById("username");
    elementUserName.addEventListener('input', function (event) {
        elementButtonConfirm.disabled = true;

        if (elementUserName.validity.valid) {
            let value = document.getElementById("username").value;
            fetch("/api/v1/user/getByLogin/"+value).then(x => {
                return x.json();}
            )
            .then(json => {
                if(json == 'json')
                {
                    console.log(json);
                    elementUserName.classList.remove("error-style");
                    document.getElementById('Try to create another login!').innerHTML = '&nbsp';
                    
                }
                else
                {
                    elementUserName.classList.add("error-style");
                    document.getElementById('Try to create another login!').innerHTML = 'Try to create another login!';

                }
            })
               
        }
      
    }, false);


    let elementPassword = document.getElementById("password");
    elementPassword.addEventListener('input', function (event) {
        elementButtonConfirm.disabled = true;

        if (elementPassword.validity.valid) {
            elementPassword.classList.remove("error-style");
            document.getElementById('passwordDiv').innerHTML = '&nbsp';
        }
        else {
            elementPassword.classList.add("error-style");
            if (elementPassword.value.length < 5) {
                if (elementPassword.value.length == 0) {
                    document.getElementById('passwordDiv').innerHTML = 'This field is required!';
                }
                else {
                    document.getElementById('passwordDiv').innerHTML = "Password must be at least 5 characters long!";
                }

            }
        }
    }, false);



    let elementConfirmPassword = document.getElementById("confirmPassword");
    elementConfirmPassword.addEventListener('input', function (event) {
        elementButtonConfirm.disabled = true;

        if (elementConfirmPassword.validity.valid) {
            elementConfirmPassword.classList.remove("error-style");
            document.getElementById('confirmPasswordDiv').innerHTML = '&nbsp';
        }
        else {
            elementConfirmPassword.classList.add("error-style");
            if (elementConfirmPassword.value.length < 5) {
                if (elementConfirmPassword.value.length == 0) {
                    document.getElementById('confirmPasswordDiv').innerHTML = 'This field is required!';
                }
                else {
                    document.getElementById('confirmPasswordDiv').innerHTML = "Password must be at least 5 characters long!";
                }

            }
        }
    }, false);


    

    let elementRegistrationForm = document.getElementById("registrationForm");
    elementRegistrationForm.addEventListener('input', function (event) {
        elementButtonConfirm.disabled = true;

        if (elementConfirmPassword.validity.valid && elementPassword.validity.valid) {
            if (elementConfirmPassword.value === elementPassword.value) {
                elementConfirmPassword.classList.remove("error-style");
                document.getElementById('confirmPasswordDiv').innerHTML = '&nbsp';

                elementPassword.classList.remove("error-style");
                document.getElementById('passwordDiv').innerHTML = '&nbsp';

        if (elementUserName.validity.valid) {
            let value = document.getElementById("username").value;
            fetch("/api/v1/user/getByLogin/"+value).then(x => {
                return x.json();}
            )
            .then(json => {
                if(json == 'json')
                {
                    console.log(json);
                    elementUserName.classList.remove("error-style");
                    document.getElementById('Try to create another login!').innerHTML = '&nbsp';
                    elementButtonConfirm.disabled = false;


                    
                }
                else
                {
                    elementUserName.classList.add("error-style");
                    document.getElementById('Try to create another login!').innerHTML = 'Try to create another login!';

                }
            })
               
        }


        }
    }
        else {

            if (!elementConfirmPassword.validity.valid) {
                elementConfirmPassword.classList.add("error-style");
                if (elementConfirmPassword.value.length < 5) {
                    if (elementConfirmPassword.value.length == 0) {
                        document.getElementById('confirmPasswordDiv').innerHTML = 'This field is required!';
                    }
                    else document.getElementById('confirmPasswordDiv').innerHTML = "Password must be at least 5 characters long!";
                }
            }
            if (!elementPassword.validity.valid) {
                elementPassword.classList.add("error-style");
                if (elementPassword.value.length < 5) {
                    if (elementPassword.value.length == 0) {
                        document.getElementById('passwordDiv').innerHTML = 'This field is required!';
                    }
                    else document.getElementById('passwordDiv').innerHTML = "Password must be at least 5 characters long!";
                }
            }
        }



    }, false);

}






























