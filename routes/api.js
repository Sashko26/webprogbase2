var express = require('express');
var router = express.Router();
const passport = require('passport');

const userJs = require('../models/user');
const postJs = require('../models/post');
const additionalJs = require('../models/additional.js');
const photoJs = require('../models/photo.js');
const commentJs = require('../models/comment');
const configJs = require('../config.js');
const serverSalt = configJs.hashingSalt;
const cloudinary = require('cloudinary').v2;
cloudinary.config({
    cloud_name: configJs.cloud_name,
    api_key: configJs.api_key,
    api_secret: configJs.api_secret
});
const auth = require('../auth');
router.use('/auth', auth);





router.get('/user/getByLogin/:login', function (req, res) {

    userJs.User.getByLogin(req.params.login)
        .then(user => {
            console.log("hi " + user);
            if (user == null) {
                res.json("json");
            }
            else {
                res.json(user);
            }
        })
        .catch(error => {
            console.log('hello')
            res.status(500).json("we have some problems")
        });
})







router.get('/users/:id', passport.authenticate('basic', { session: false }),
    function (req, res) {

        if (req.user.role == 'admin' || req.params.id == req.user._id) {
            userJs.User.getById(req.params.id)
                .then(user => { res.json(user); })
                .catch(error => { res.send(error) })
        }
        else {
            res.status(403).send("You have not rights!)");
        }

    });
router.post('/users', passport.authenticate('basic', { session: false }), function (req, res) {

    let hashedPass = sha512(req.body.password, serverSalt).passwordHash;
    if (req.user.role == 'admin') {
        if (req.body.login && req.body.password) {
            if (req.files.avatar) {
                new Promise((resolve, reject) => {
                    cloudinary.uploader.upload_stream({ resource_type: 'raw' }, function (error, result) {
                        if (error)
                            reject(error)
                        else
                            resolve(result)
                    }).end(req.files.avatar.data)
                }).then(result => {
                    return {
                        login: req.body.login,
                        hashedPassword: hashedPass,
                        name: req.body.name,
                        avaUrl: result.secure_url,
                        biography: req.body.biography,
                        public_id: result.public_id
                    }
                }).then(user => userJs.User.insert(user))
                    .then(user => {

                        res.status(201).redirect('/api/v1/users/' + user._id)
                    })
                    .catch(error => { res.status(500).json(error) });
            }
            else {
                let user = {
                    login: req.body.login,
                    hashedPassword: hashedPass,
                    name: req.body.name,
                    biography: req.body.biography
                }
                userJs.User.insert(user)

                    .then(user => {

                        res.redirect('/api/v1/users/' + user._id)
                    })
                    .catch(error => { res.status(500).json(error) })
            }

        }
        else {
            res.status(400).json('User must have password and login!');
        }
    }
    else {
        res.status(403).json('You have not righsts to create user!');
    }
})

router.delete('/users/:id', passport.authenticate('basic', { session: false }), function (req, res) {
    if (req.user.role == 'admin' || req.user._id == req.params.id) {
        Promise.all([userJs.User.delete(req.params.id), postJs.Post.deleteAllPostsThatBelongUser(req.params.id), commentJs.Comment.deleteAllCommentsThatBelongUser(req.params.id)])
            .then((deletedUser) => {
                if (!deletedUser[0]) {
                    res.status(404);
                    return Promise.reject('User with such ID not exists!');
                }
                else if (!deletedUser[0].public_id) {
                    let deletedUserObj = { deletedUser: deletedUser[0], message: "successfully deleted" };
                    res.status(200).json(deletedUserObj);
                }

                else {
                    return new Promise((resolve, reject) => {
                        cloudinary.uploader.destroy(deletedUser[0].public_id, { invalidate: true, resource_type: "raw" }, function (error, result) {
                            if (error)
                                return reject(error)
                            else
                                return resolve(deletedUser)
                        })
                    })
                }
            })

            .then(deletedUser => {

                let deletedUserObj = { deletedUser: deletedUser, message: "successfully deleted" };
                res.status(200).json(deletedUserObj);
            })
            .catch(error => {

                res.json(error);
            })
    }
    else {
        res.status(403).json('You have not rights to create user!');
    }
})

router.put('/users/:id', passport.authenticate('basic', { session: false }), function (req, res) {

    if (req.user.role == 'admin' || req.user._id == req.params.id) {
        if (req.files.avatar) {
            let globalValueRole;
            let globalValueLogin;
            let globalValueHashedPassword;
            let globalValueName;
            let globalValueSurname;

            let globalValueId;
            let globalValueEmail;

            let user = new Promise((resolve, reject) => {
                userJs.User.getById(req.params.id)
                    .then(user => {
                        globalValueId = user.id;
                        globalValueRole = user.role;
                        globalValueLogin = user.login;
                        globalValueHashedPassword = user.hashedPassword;
                        globalValueName = user.name;
                        globalValueSurname = user.surname;

                        globalValueEmail = user.email;
                        resolve(user)
                    })
                    .catch(error => reject(error))
            })
            user.then(userGetFromDb => {

                if (userGetFromDb.public_id) {
                    return new Promise((resolve, reject) => {

                        cloudinary.uploader.destroy(userGetFromDb.public_id, { invalidate: true, resource_type: "raw" }, function (error, result) {
                            if (error)
                                reject(error)
                            else
                                resolve(result)
                        })
                    })
                }
                else return Promise.resolve("good");

            })
                .then(resultOfDeliting => {

                    return new Promise((resolve, reject) => {
                        cloudinary.uploader.upload_stream({ resource_type: 'raw' }, function (error, result) {
                            if (error)
                                reject(error)
                            else
                                resolve(result)
                        }).end(req.files.avatar.data);
                    })

                })
                .then(resultOfUploading => {

                    let role = req.body.role;
                    let login = req.body.login;
                    let password = req.body.password;
                    let name = req.body.name;
                    let email = req.body.email;
                    let hashedPassword;
                    if (req.body.email = undefined)
                        if (req.body.password) {
                            hashedPassword = sha512(password, serverSalt).passwordHash;
                        }
                    if (req.body.login == undefined) {
                        login = globalValueLogin;
                    }
                    if (req.body.password == undefined) {
                        hashedPassword = globalValueHashedPassword;
                    }
                    if (req.body.name == undefined) {
                        name = globalValueName;
                    }
                    if (req.body.name == undefined) {
                        name = globalValueSurname;
                    }
                    if (req.body.email == undefined) {
                        email = globalValueEmail;
                    }


                    if (req.body.role == undefined) {
                        role = globalValueRole;
                    }
                    let UpdatedUser;
                    if (req.user.role != 'admin') {
                        role = globalValueRole;
                    }
                    UpdatedUser = { id: globalValueId, login: login, hashedPassword: hashedPassword, name: name, email: email, role: role, avaUrl: resultOfUploading.secure_url, public_id: resultOfUploading.public_id };

                    return userJs.User.update(UpdatedUser);
                })
                .then(UpdatedUserFromDB => {

                    res.status(200).json(UpdatedUserFromDB)
                }
                )
                .catch(error => {
                    res.status(500).json(error);
                })
        }
        if (!req.files.avatar) {

            userJs.User.getById(req.params.id)
                .then(user => {

                    let role = req.body.role;
                    let login = req.body.login;
                    let password = req.body.password;
                    let name = req.body.name;
                    let biography = req.body.biography;
                    let hashedPassword;
                    if (req.body.password) {
                        hashedPassword = sha512(password, serverSalt).passwordHash;
                    }
                    if (!req.body.password) {
                        hashedPassword = user.hashedPassword;
                    }

                    if (req.body.login == undefined) {
                        login = user.login;
                    }
                    if (req.body.password == undefined) {
                        hashedPassword = user.hashedPassword;
                    }
                    if (req.body.name == undefined) {
                        name = user.name;
                    }

                    if (req.body.biography == undefined) {
                        biography = user.biography;
                    }
                    if (req.body.role == undefined) {
                        role = user.role;
                    }
                    let UpdatedUser;
                    if (req.user.role != 'admin') {
                        role = user.role;
                    }
                    UpdatedUser = { id: user.id, login: login, hashedPassword: hashedPassword, name: name, biography: biography, role: role, avaUrl: user.avaUrl, public_id: user.public_id };
                    return userJs.User.update(UpdatedUser);

                })
                .then(UpdatedUserFromDB => {
                    res.status(200).json(UpdatedUserFromDB)
                }
                )
                .catch(error => {
                    res.status(500).json(error);
                })

        }
    }
    else {
        res.status(403).json('You have not righsts to update user!');
    }
})


//Crud Post

router.get('/posts/:id', passport.authenticate('basic', { session: false }), function (req, res) {
    postJs.Post.getById(req.params.id)
        .then(post => {
            if (post) { res.status(200).json(post) }
            if (!post) { res.status(404).json("Post not found!") }
        })
        .catch(error => { res.status(500).json(error) })
})

router.post('/posts', passport.authenticate('basic', { session: false }), function (req, res) {
    let name = req.body.name;
    let content = req.body.content;
    if (name == undefined) {
        res.status(400).json({ error: "Fill name field!" });
        return;
    }
    if (content == undefined) {
        res.status(400).json({ error: "Fill content field!" });
        return;
    }
    if (req.body.content.length > 200) {
        req.body.content = req.body.content.substring(0, 200);
    }

    if (req.files.photoFile) {
        new Promise((resolve, reject) => {
            cloudinary.uploader.upload_stream({ resource_type: 'raw' }, function (error, result) {
                if (error)
                    reject(error)
                else
                    resolve(result)
            }).end(req.files.photoFile.data)
        }).then(result => {
            if (!result) {
                res.status(500)
                return Promise.reject({ error: "Problem with connecting to cloudinary!" });
            }
            else return additionalJs.getPostFromRequestFromCreatingNewPost(req.user.id, req.body.name, result.secure_url, req.body.content, result.public_id);
        }).then(result => {
            res.status(201).json(result);
        }).catch(err => {
            res.json(err);
            return;
        });
    }
    else {
        let post = { idOfOwner: req.user.id, name: req.body.name, content: req.body.content };
        postJs.Post.insert(post)
            .then(result => {
                res.status(201).json(result);
            }).catch(err => {
                res.status(500).send("Some problems with DB!");
                return;
            });

    }
})
router.put('/posts/:id', passport.authenticate('basic', { session: false }), function (req, res) {


    if (req.files.photoFile) {
        let globalValueContent;
        let globalValueName;
        let post = new Promise((resolve, reject) => {
            postJs.Post.getById(req.params.id)
                .then(post => {
                    if (!post) {
                        res.status(404);
                        return Promise.reject("Post not Found!");
                    }
                    else if (req.user.role != 'admin' && post.idOfOwner != req.user.id) {
                        res.status(403);
                        return Promise.reject("You have not rights!");
                    }
                    globalValueContent = post.content;
                    globalValueName = post.name;
                    resolve(post)
                })
                .catch(error => {
                    res.status(500);
                    reject(error);
                })
        })
        post.then(postGetFromDb => {

            return new Promise((resolve, reject) => {
                if (postGetFromDb.public_id) {
                    cloudinary.uploader.destroy(postGetFromDb.public_id, { invalidate: true, resource_type: "raw" }, function (error, result) {
                        if (error) {
                            res.status(500);
                            reject({ error: "problem with connect to cloudinary!" });
                        }
                        else resolve(result);
                    })
                }
                else resolve("We have not what to delete from Cloudinary)");

            })
        })
            .then(resultOfDeliting => {
                return new Promise((resolve, reject) => {
                    cloudinary.uploader.upload_stream({ resource_type: 'raw' }, function (error, result) {
                        if (error) {
                            status(500);
                            reject({ error: "problem with connect to cloudinary!" })
                        }

                        else resolve(result)
                    }).end(req.files.photoFile.data);
                })

            })
            .then(resultOfUploading => {
                let content = req.body.content;
                let name = req.body.name;
                if (req.body.name == undefined) name = globalValueName;
                if (req.body.content == undefined) content = globalValueContent;

                let updatedPost = { id: req.params.id, name: name, content: content, pictureUrl: resultOfUploading.secure_url, public_id: resultOfUploading.public_id };
                return postJs.Post.update(updatedPost);
            })
            .then(UpdatedPostFromDB => {
                res.status(200).json(UpdatedPostFromDB);
            }
            )
            .catch(error => {
                res.json(error);
            })
    }
    if (!req.files.photoFile) {

        postJs.Post.getById(req.params.id)
            .then(post => {
                if (!post) {
                    res.status(404);
                    return Promise.reject("Post not Found!");
                }
                let name = req.body.name;
                let content = req.body.content;
                if (req.body.name == undefined) {
                    name = post.name;
                }
                if (req.body.content == undefined) {
                    content = post.content;
                }
                let UpdatedPost = { id: req.params.id, name: name, content: content };

                return postJs.Post.update(UpdatedPost);

            })
            .then(oldPostFromDB => {
                return postJs.Post.getById(oldPostFromDB.id);
            })
            .then(UpdatedPostFromDB => {
                res.status(200).json(UpdatedPostFromDB);
            })
            .catch(error => {
                res.json(error);
            })
    }


})

router.delete('/posts/:id', passport.authenticate('basic', { session: false }), function (req, res) {
    commentJs.Comment.deleteAllCommentsThatBelongPost(req.params.id)
        .then(result => {

            return postJs.Post.getById(req.params.id)
        })
        .then(post => {
            if (!post) { res.status(404); return Promise.reject("Post with such ID not exists in dataBase!"); }
            else if (req.user.role != 'admin' && post.idOfOwner != req.user.id) {
                res.status(403);
                return Promise.reject("You have not rights!");
            }
            else {
                if (post.idOfOwner != req.user.id && req.user.role != 'admin') { res.status(403).json("You have not right to delete this post!"); }
                else { return postJs.Post.deleteById(post.id) }
            }
        })
        .then(deletedPost => {
            if (!deletedPost) {
                res.status(500);
                Promise.reject("During our collaboration, the post disappeared somewhere!");

            }
            else if (deletedPost.public_id) {
                return new Promise((resolve, reject) =>
                    cloudinary.uploader.destroy(deletedPost.public_id, { invalidate: true, resource_type: "raw" }, function (error, result) {
                        if (error)
                            return reject(error)
                        else
                            return resolve(deletedPost)
                    }))
                    .then(deletedPost => {
                        let objDeletedPost = { deletedPost: deletedPost, message: "successfully deleted" }
                        res.status(200).json(objDeletedPost);
                    })
            }
            else {
                res.status(200).json({ deletedPost: deletedPost, message: "successfully deleted" });
            }
        })
        .catch(err => res.json(err))
})

//Crud Comment
router.get('/comments/:id', passport.authenticate('basic', { session: false }), function (req, res) {

    commentJs.Comment.getById(req.params.id)
        .then(comment => {
            if (!comment) {
                res.status(404)
                return Promise.reject("Comment with such ID not found!");
            }
            else {
                res.status(200).json(comment);
            }
        })
        .catch(err => res.json(err));
})



router.get('/commentsLocalStrategy', function (req, res) {
    if (true/* req.user */) {

        if (req.user.role == 'admin') {
            let pageSize = 5;
            let limit;
            if (req.query.page) {
                limit = (req.query.page) * pageSize;
            }
            else {
                limit = pageSize;
                req.query.page = 1;
            }
            let idOfPost = req.query.idOfPost;

            console.log(idOfPost + "buyaka show");
            console.log(idOfPost + " huy v cholo");

            commentJs.Comment.getAllWithPagination(limit, idOfPost)
                .then(comments => {



                    for (let i = 0; i < comments.length / 2; i++) {
                        let buf = comments[i];
                        console.log(comments[comments.length - 1 - i] + "last");
                        console.log(comments[i] + "first");
                        comments[i] = comments[comments.length - 1 - i]
                        /*  console.log(comments[i]) */
                        comments[comments.length - 1 - i] = buf;


                        console.log(comments[comments.length - 1 - i] + "last");
                        console.log(comments[i] + "first");




                    }
                    console.log(comments);

                    for (let i = 0; i < comments.length; i++) {
                        comments[i].deleteIcon = true;
                    }


                    console.log(comments);

                    let commentsObj = {};
                    if (req.query.page) {
                        commentsObj['items'] = comments;

                        commentsObj["nextPage"] = parseInt(req.query.page) + 1;
                        commentsObj["prevPage"] = parseInt(req.query.page) - 1;
                        commentsObj["page"] = parseInt(req.query.page);

                    }





                    commentsObj.admin = true;



                    let arrayOfPromises = [];
                    for (let i = 0; i < commentsObj.items.length; i++) {
                        arrayOfPromises[i] = userJs.User.getById(commentsObj.items[i].idOfOwner);
                    }


                    return Promise.all([commentsObj, arrayOfPromises]);
                })
                .then(([commentsObj, arrayOfPromises]) => {
                    return Promise.all(arrayOfPromises)
                        .then(arrayOfPromises => {

                            for (let i = 0; i < commentsObj.items.length; i++) {
                                commentsObj.items[i].avaUrl = arrayOfPromises[i].avaUrl;
                                console.log(commentsObj.items[i].avaUrl);
                                commentsObj.items[i].userLogin = arrayOfPromises[i].login;
                                console.log(commentsObj.items[i].userLogin);


                            }
                            res.json(commentsObj);
                        })
                })

                .catch(error => { res.send(error) })
        }
        else if (req.user.role = 'user') {
            let pageSize = 5;
            let limit;
            if (req.query.page) {
                limit = (req.query.page) * pageSize;
            }
            else {
                limit = pageSize;
                req.query.page = 1;
            }
            let idOfPost = req.query.idOfPost;

            commentJs.Comment.getAllWithPagination(limit, idOfPost)
                .then(comments => {

                    for (let i = 0; i < comments.length / 2; i++) {
                        let buf = comments[i];
                        console.log(comments[comments.length - 1 - i] + "last");
                        console.log(comments[i] + "first");
                        comments[i] = comments[comments.length - 1 - i]
                        /*  console.log(comments[i]) */
                        comments[comments.length - 1 - i] = buf;


                        console.log(comments[comments.length - 1 - i] + "last");




                    }
                    for (let i = 0; i < comments.length; i++) {
                        if (req.user.id == comments[i].idOfOwner) {
                            comments[i].deleteIcon = true;
                        }
                    }

                    console.log(comments);
                    let commentsObj = {};
                    if (req.query.page) {
                        commentsObj['items'] = comments;

                        commentsObj["nextPage"] = parseInt(req.query.page) + 1;
                        commentsObj["prevPage"] = parseInt(req.query.page) - 1;
                        commentsObj["page"] = parseInt(req.query.page);

                    }



                    commentsObj.admin = true;



                    let arrayOfPromises = [];
                    for (let i = 0; i < commentsObj.items.length; i++) {
                        arrayOfPromises[i] = userJs.User.getById(commentsObj.items[i].idOfOwner);
                    }


                    return Promise.all([commentsObj, arrayOfPromises]);
                })
                .then(([commentsObj, arrayOfPromises]) => {
                    return Promise.all(arrayOfPromises)
                        .then(arrayOfPromises => {

                            for (let i = 0; i < commentsObj.items.length; i++) {
                                commentsObj.items[i].avaUrl = arrayOfPromises[i].avaUrl;
                                commentsObj.items[i].userLogin = arrayOfPromises[i].login;


                            }
                            res.json(commentsObj);
                        })
                })

                .catch(error => { res.send(error) })
        }

    }
    else {
        res.redirect('/auth/register');
    }
});



router.get('/photosLocalStrategy', function (req, res) {
    if (true/* req.user */) {

        if (req.user.role == 'admin') {
            let pageSize = 5;
            let limit;
            if (req.query.page) {
                limit = (req.query.page) * pageSize;
            }
            else {
                limit = pageSize;
                req.query.page = 1;
            }
            let idOfUser = req.query.idOfUser;

            console.log(idOfUser + "buyaka show");
            console.log(idOfUser + " huy v cholo");

            photoJs.Photo.getAllWithPagination(limit, idOfUser)
                .then(comments => {



                    for (let i = 0; i < comments.length / 2; i++) {
                        let buf = comments[i];
                      
                        comments[i] = comments[comments.length - 1 - i]
                        comments[comments.length - 1 - i] = buf;
                      
                    }
                
                    for (let i = 0; i < comments.length; i++) {
                        comments[i].deleteIcon = true;
                    }

                   
                    let commentsObj = {};
                    if (req.query.page) {
                        commentsObj['items'] = comments;

                        commentsObj["nextPage"] = parseInt(req.query.page) + 1;
                        commentsObj["prevPage"] = parseInt(req.query.page) - 1;
                        commentsObj["page"] = parseInt(req.query.page);

                    }
                    commentsObj.admin = true;
                    res.json(commentsObj);
                })

                .catch(error => { res.send(error) })
        }
        else if (req.user.role = 'user') {
            let pageSize = 5;
            let limit;
            if (req.query.page) {
                limit = (req.query.page) * pageSize;
            }
            else {
                limit = pageSize;
                req.query.page = 1;
            }
            let idOfUser = req.query.idOfUser;

          

            photoJs.Photo.getAllWithPagination(limit, idOfUser)
                .then(comments => {



                    for (let i = 0; i < comments.length / 2; i++) {
                        let buf = comments[i];
                        console.log(comments[comments.length - 1 - i] + "last");
                        console.log(comments[i] + "first");
                        comments[i] = comments[comments.length - 1 - i]
                        comments[comments.length - 1 - i] = buf;
                        console.log(comments[comments.length - 1 - i] + "last");
                        console.log(comments[i] + "first");
                    }
                   
                    for (let i = 0; i < comments.length; i++) {
                        comments[i].deleteIcon = true;
                    }

                

                    let commentsObj = {};
                    if (req.query.page) {
                        commentsObj['items'] = comments;

                        commentsObj["nextPage"] = parseInt(req.query.page) + 1;
                        commentsObj["prevPage"] = parseInt(req.query.page) - 1;
                        commentsObj["page"] = parseInt(req.query.page);

                    }
                    
                    commentsObj.admin = true;
                    res.json(commentsObj);
                })

                .catch(error => { res.send(error) })
        }
    }
    else {
        res.redirect('/auth/register');
    }
});















router.post('/comments/', passport.authenticate('basic', { session: false }), function (req, res) {

    if (req.body.name == undefined) {
        res.status(400).json({ error: "Fill name field!" });
        return;
    }
    if (req.body.content == undefined) {
        res.status(400).json({ error: "Fill content field!" });
        return;
    }
    if (req.body.content.length > 200) {
        req.body.content = req.body.content.substring(0, 200);
    }
    if (req.files.photoFile) {
        new Promise((resolve, reject) => {
            cloudinary.uploader.upload_stream({ resource_type: 'raw' }, function (error, result) {
                if (error)
                    reject(error)
                else
                    resolve(result)
            }).end(req.files.photoFile.data)
        }).then(result => {
            if (!result) {
                res.status(500)
                return Promise.reject({ error: "Problem with connecting to cloudinary!" });
            }
            else {
                let x = { idOfOwner: req.user.id, name: req.body.name, pictureUrl: result.secure_url, content: req.body.content, public_id: result.public_id };
                return commentJs.Comment.insert(x)
            }
        }).then(result => {

            res.status(201).json(result);

        }).catch(err => {

            res.json(err);
            return;
        });
    }
    else {
        let comment = { idOfOwner: req.user.id, name: req.body.name, content: req.body.content };
        commentJs.Comment.insert(comment)
            .then(result => {
                res.status(201).json(result);
            }).catch(err => {

                res.status(500).send("Some problems with DB!");
                return;
            });

    }

})

router.put('/comments/:id', passport.authenticate('basic', { session: false }), function (req, res) {


    if (req.files.photoFile == undefined) {
        commentJs.Comment.getById(req.params.id)
            .then(comment => {
                if (!comment) {
                    res.status(404)
                    return Promise.reject("Comment with such ID not found!");
                }
                else if (req.user.role != 'admin' && req.user.id != comment.idOfOwner) {
                    res.status(403);
                    return Promise.reject('You have not rights!');
                }
                else return Promise.resolve(comment);
            })
            .then(comment => {
                let name, content;
                if (req.body.name == undefined)
                    name = comment.name;
                else name = req.body.name;
                if (req.body.content == undefined)
                    content = comment.content;
                else content = req.body.content;
                let x = { id: comment.id, name: name, content: content }

                return commentJs.Comment.update(x)
            })
            .then(oldComment => {
                return commentJs.Comment.getById(oldComment.id);
            })
            .then(updatedComment => {
                let objUpdatedComment = { updatedComment: updatedComment, message: "successfully updated" };
                res.status(200).json(objUpdatedComment);
            })
            .catch(err => res.json(err));
    }
    else {
        commentJs.Comment.getById(req.params.id)
            .then(comment => {
                if (!comment) {
                    res.status(404)
                    return Promise.reject("Comment with such ID not found!");
                }
                else if (req.user.role != 'admin' && req.user.id != comment.idOfOwner) {
                    res.status(403);
                    return Promise.reject('You have not rights!');
                }
                return new Promise((resolve, reject) => {
                    if (comment.public_id) {
                        cloudinary.uploader.destroy(comment.public_id, { invalidate: true, resource_type: "raw" }, function (error, result) {
                            if (error) {
                                res.status(500);
                                return reject('Some problem with connect to cloudinary!');
                            }
                            else return resolve(comment);
                        })

                    }
                    else {
                        return resolve(comment);
                    }

                })

            })
            .then(comment => {
                return new Promise((resolve, reject) => {
                    cloudinary.uploader.upload_stream({ resource_type: 'raw' }, function (error, result) {
                        if (error) {
                            res.status(500);
                            return reject("Problem with connecting to cloudinary!");
                        }
                        else {
                            let array = [comment, result];
                            resolve(array);
                        }

                    }).end(req.files.photoFile.data);
                })
            })
            .then(result => {

                let pictureUrl = result[1].secure_url;
                let public_id = result[1].public_id;
                let name, content;
                if (req.body.name == undefined)
                    name = result[0].name;
                else name = req.body.name;
                if (req.body.content == undefined)
                    content = result[0].content;
                else content = req.body.content;

                let x = { id: req.params.id, name: name, content: content, pictureUrl: pictureUrl, public_id: public_id };

                return commentJs.Comment.update(x)
            })
            .then(oldComment => {

                return commentJs.Comment.getById(oldComment.id)
            })
            .then(updatedComment => {
                let objUpdatedComment = { updatedComment: updatedComment, message: "successfully updated" };

                res.status(200).json(objUpdatedComment);
            })
            .catch(err => { res.json(err) });
    }
});
router.delete('/comments/:id', passport.authenticate('basic', { session: false }), function (req, res) {
    commentJs.Comment.getById(req.params.id)
        .then(comment => {
            if (!comment) {
                res.status(404);
                return Promise.reject(comment);
            }
            else {
                if (comment.idOfOwner != req.user.id && req.user.role != 'admin') {
                    res.status(403);
                    return Promise.reject("You have not right to delete this post!")
                }
                else {
                    return commentJs.Comment.deleteById(comment.id)
                }
            }
        })
        .then(deletedComment => {
            if (!deletedComment) {
                res.status(500);
                return Promise.reject("During our collaboration, the post disappeared somewhere!");
            }
            else {
                res.status(200).json({ deletedComment: deletedComment, message: "successfully deleted" });
            }

        })
        .catch(err => res.json(err))
})

//get
router.get('', function (req, res) {
    res.set({ "Content-type": "application/json" }).status(200).send('');
})
/////
router.get('/me',
    passport.authenticate('basic', { session: false }),
    function (req, res) {
        res.json(req.user);
    });
router.get('/users', passport.authenticate('basic', { session: false }),
    function (req, res) {
        if (req.user) {
            if (req.user.role == 'admin') {

                userJs.User.getAll()
                    .then(users => {
                        let usersObj = {};
                        usersObj['items'] = users;
                        usersObj = additionalJs.PaginationAndSearch(req, usersObj, 'user');
                        console.log(usersObj);
                        users = usersObj.items;
                        res.json(usersObj);

                    })
                    .catch(error => { res.send(error) })
            }
            else {

                res.status(403).json("You have not rights!)");
            }

        }
    });






// identical request with different strategy 
router.get('/postsLocalStrategy', function (req, res) {
    if (true/* req.user */) {
        if (req.user.role == 'admin') {
            postJs.Post.getAll()
                .then(
                    posts => {
                        let postsObj = {};
                        postsObj['items'] = posts;
                        postsObj = additionalJs.PaginationAndSearch(req, postsObj, 'post');
                        res.json(postsObj);
                    })

                .catch(error => { res.status(500).send(error) })
        }
        else if (req.user.role = 'user') {
            postJs.Post.getAll()
                .then(
                    posts => {

                        let postsObj = {};
                        postsObj['items'] = posts;
                        if (postsObj.items.length == 0) {
                            postsObj.isEmpty = true;

                        }
                        postsObj = additionalJs.PaginationAndSearch(req, postsObj, 'post');
                        res.json(postsObj);
                    })

                .catch(error => {
                    res.status(500).send(error)
                })
        }

    }
    else {
        res.redirect('/auth/register');
    }
});



router.get('/posts', passport.authenticate('basic', { session: false }), function (req, res) {

    if (req.user.role == 'admin') {
        postJs.Post.getAll()
            .then(
                posts => {
                    let postsObj = {};
                    postsObj['items'] = posts;
                    postsObj = additionalJs.PaginationAndSearch(req, postsObj, 'post');
                    /* posts = postsObj.items; */
                    res.json(postsObj);
                })

            .catch(error => { res.send(error) })
    }
    else if (req.user.role = 'user') {
        postJs.Post.getAllThatBelongToUser(req.user.id)
            .then(
                posts => {
                    let postsObj = {};
                    postsObj['items'] = posts;
                    postsObj = additionalJs.PaginationAndSearch(req, postsObj);
                    posts = postsObj.items;
                    res.json(posts);
                })

            .catch(error => { res.send(error) })
    }
});
/////////////////













router.get('/comments', passport.authenticate('basic', { session: false }),
    function (req, res) {
        if (true/* req.user */) {
            if (req.user.role == 'admin') {
                commentJs.Comment.getAll()
                    .then(comments => {
                        let commentsObj = {};
                        commentsObj['items'] = comments;
                        commentsObj = additionalJs.PaginationAndSearch(req, commentsObj, 'comment');
                        commentsObj.admin = true//req.user.login;


                        let arrayOfPromises = [];
                        for (let i = 0; i < commentsObj.items.length; i++) {
                            arrayOfPromises[i] = userJs.User.getById(commentsObj.items[i].idOfOwner);
                        }


                        return Promise.all([commentsObj, arrayOfPromises]);
                    })
                    .then(([commentsObj, arrayOfPromises]) => {
                        return Promise.all(arrayOfPromises)
                            .then(arrayOfPromises => {

                                for (let i = 0; i < commentsObj.items.length; i++) {
                                    /*  commentsObj.items[i].avaUrl = arrayOfPromises[i].avaUrl; */
                                    commentsObj.items[i].userLogin = arrayOfPromises[i].login;
                                    /*  console.log(commentsObj.logins[i] + "$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                                     console.log(commentsObj.items[i].name); */

                                }
                                /* console.log(commentsObj.logins[0])  */


                                res.json(commentsObj);
                            })
                    })

                    .catch(error => { res.send(error) })
            }
            else if (req.user.role = 'user') {
                commentJs.Comment.getAllThatBelongToUser(req.user.id)
                    .then(
                        posts => {
                            let postsObj = {};
                            postsObj['items'] = posts;
                            postsObj = additionalJs.PaginationAndSearch(req, postsObj);
                            posts = postsObj.items;
                            res.json(posts);
                        })

                    .catch(error => {
                        console.log("hi little pidar!!!!!!!!!");
                        res.send(error)
                    })
            }

        }
        else {
            res.redirect('/auth/register');
        }
    });




module.exports = router;