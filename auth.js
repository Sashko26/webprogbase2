const express = require('express');
const router = express.Router();
const passportModule = require("./passportModule");
const passport = require("passport");
const configJs = require('./config.js');
const userJs = require('./models/user.js');
const serverSalt = configJs.hashingSalt;

router.post('/login',
  passport.authenticate('local', {
    successRedirect: '/', // Get /
    failureRedirect: '/auth/login?error=User+exists+already!'
  }), function (req, res) {

  }); // get login 

// вихід із сесії
router.get('/logout',
  (req, res) => {
    req.logout();
    res.redirect('/');
  });


function sha512(password, salt) {
  const hash = crypto.createHmac('sha512', salt);
  hash.update(password);
  const value = hash.digest('hex');
  return {
    salt: salt,
    passwordHash: value
  };
}



router.get('/register', function (req, res) {

  if (req.query.error) {
    if (req.query.error == "Passwords are different!") {

      /* let obj = {errorDiffpass:req.query.error,guets:'huyaka'} */
      let errorDiffpass = req.query.error;
      res.render('register', { errorDiffpass: errorDiffpass });
    }
    else {

      let error = req.query.error;
      res.render('register', { error: error, guets: 'guest' });
    }
  }
  else
    res.render('register', { guest: 'huyaka' });
});












router.post('/register', (req, res) => {
  // @todo перевірити валідність даних і створити нового користувача у БД 

  let username = req.body.username;
  let pass = req.body.pass;
  let pass2 = req.body.pass2;
  if (pass !== pass2) {

    res.redirect('/auth/register?error=Passwords+are+different!');
  }

  else {
    const hashedPass = passportModule.sha512(pass, serverSalt).passwordHash;

    userJs.User.getByLogin(username)
      .then(user => {
        if (user === null) {

          let x = { login: username, hashedPassword: hashedPass };
          userJs.User.insert(x)
            .then(user => res.redirect('login'))
            .catch(err => { console.log(err) });

        }
        else {
          res.redirect('/auth/register?error=such+login+exists+already!');
        }
      })

  }


});

router.get('/login', function (req, res) {


  if (req.query.error) {

    let error = req.query.error;
    res.render('login', { error: error, guest: 'guest' });
  }
  else
    res.render('login', { guest: 'guest' });

});

router.post('/logout',
  (req, res) => {
    req.logout();
    res.redirect('/');
  });




module.exports = router;

